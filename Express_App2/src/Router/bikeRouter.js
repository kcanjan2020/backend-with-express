import { Router } from "express";

let bikeRouter=Router();
bikeRouter.route("/")

//.post(fun1,fun2,fun3);

// Normal Middleware(req,res,next)==>To trigger we have call next()
//Error Middleware(err,req,res,next)==>To trigger we have to call next(variable//or data//or error)

//.post(fun1,fun2,fun3)
.post((req,res,next)=>{
    console.log("I am Middleware 1");
    next();
},
(req,res,next)=>{
    console.log("I am Middleware 2");
    next()
},
(req,res,next)=>{
    console.log("I am Middleware 3");
})

.get((req,res,next)=>{
    res.json("bike get");

})

.patch((req,res)=>{
    res.json("bike patch")
})
.delete((req,res)=>{
    res.json("bike delete")
    
})

export default bikeRouter;