
import { Router } from "express";

let firstRouter=Router();
firstRouter.route("/") //here "/"(slash) indicate that=> localhost:8000 which is defined in port in index.js
.post((req,res)=>{
    console.log("Body Data",req.body);
    console.log("Query Data",req.query);
    res.json("Home Post Successfully");
})

firstRouter.route("/name") /// localhost:8000/name =>while selecting api it sees route it does not sees url
.post((req,res)=>{

    console.log("Data",req.body);
    res.json("Name Post Successfully");
})

//Dynamic Route Parameter
firstRouter.route("/a/:country/b/:name")
.post((req,res)=>{
    console.log(req.params);
    //output:{ country: 'china', name: 'Anjan' }
})
firstRouter.route("/a/:any1/b/name/:any2")
.post((req,res)=>{
    console.log(req.params)
    res.json("Hello from Dynamic Route")
})
export default firstRouter;