import express, { json } from "express"
import firstRouter from "./src/Router/firstRouter.js";
import bikeRouter from "./src/Router/bikeRouter.js";

let expressApp=express();
expressApp.use(json());
let port=8000;

expressApp.listen(port,()=>{
    console.log(`Express App is Listening at port ${port}`)
})

//Application Middleware
//expressApp.use(func1,func2,func3);
expressApp.use(
    (req,res,next)=>{
        console.log(" I am application middleware 1");
        next();

    },
    (req,res,next)=>{
        console.log("I am application middleware 2");
        next();

    },
    (req,res,next)=>{
        console.log("I am application middleware 3");
        next();
        
    }
)


//import firstRouter
expressApp.use("/",firstRouter)

//import bike Router
expressApp.use("/bike",bikeRouter)