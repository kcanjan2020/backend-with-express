import express, { json } from "express";
import connectToMongoDb from "./src/databaseConnection/mongoDbConnection.js";
import studentRouter from "./src/router/studentRouter.js";
import teacherRouter from "./src/router/teacherRouter.js";
let expressApp=express();
expressApp.use(json());

let port = 8000;
expressApp.listen(port,()=>{
    console.log(`Express app is listening at port ${port}`)
})

connectToMongoDb();

expressApp.use("/students",studentRouter);
expressApp.use("/teachers",teacherRouter);
