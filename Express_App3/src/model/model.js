

import { model } from "mongoose";
import studentSchema from "../schema/studentSchema.js";
import teacherSchema from "../schema/teacherSchema.js";
import bookSchema from "../schema/bookSchema.js";
import traineeSchema from "../schema/traineeSchema.js";

// Defining array is called model
export let Student=model("Student",studentSchema)
//model name must be singular and firstLetter capital
//match

//Teacher model : Use teacherSchema as as object
export let Teacher=model("Teacher",teacherSchema)

export let Book=model("Book",bookSchema)

let Trainee=model("Trainee",traineeSchema)