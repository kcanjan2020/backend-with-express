import { Router } from "express";
import { Teacher } from "../model/model.js";

let teacherRouter=Router();
teacherRouter.route("/")

.post(async(req,res)=>{
    //console.log(req.body);
    let data=req.body;
    try {
        let result= await Teacher.create(data)
        res.json({
            success:true,
            message:"Teacher Created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

})

.get(async(req,res)=>{

 try {
     let result= await Teacher.find({})
     res.json({
        success:true,
        message:"Teacher read successfully",
        result:result
     })
    
 } catch (error) {
    res.json({
        success:false,
        message:error.message
    })
    
 }

})

//for delete by Id
teacherRouter.route("/:teacherId")
.delete(async(req,res)=>{
    //console.log(req.params);
    let teacherId=req.params.teacherId;
    console.log(teacherId);
    try {
        let result= await Teacher.findByIdAndDelete(teacherId)
        res.json({
            success:true,
            message:"Teacher deleted Successfully",
            result:result
        })
        
    } catch (error) {
        
        res.json({
            success:false,
            message:error.message
        })
    }


})

//for update by id 
.patch(async(req,res)=>{
    let teacherId=req.params.teacherId;
    let data=req.body;
    try {
        let result= await Teacher.findByIdAndUpdate(teacherId,data)
        res.json({
            success:true,
            message:"Teacher updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

})

//for find by Id
.get(async(req,res)=>{
    let teacherId=req.params.teacherId;
    try {
        let result= await Teacher.findById(teacherId)
        res.json({
            success:true,
            message:"Teacher read Successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

})
export default teacherRouter;