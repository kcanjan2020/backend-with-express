import { Router } from "express";
import { Student } from "../model/model.js";
import studentSchema from "../schema/studentSchema.js";
let  studentRouter=Router();
studentRouter.route("/") //localhost:8000/students
.post(async(req,res)=>{
    let data=req.body;
    //console.log(data)
    try {
        //Save data to student
    let result= await Student.create(data)
    res.json({
        success:true,
        message:"Student created Successfully",
        result:result
    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
})
.get(async(req,res)=>{
    try {
        let result=await Student.find({})
        res.json({
        success:true,
        message:"Student Read Successfully",
        result:result,
    })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message


        })
        
        
    }
    
})
.patch((req,res)=>{
    res.json({
        success:true,
        message:"Student updated Successfully"
    })
})
.delete((req,res)=>{
    res.json({
        success:true,
        message:"Student deleted Successfully"
    })
})


//For update
studentRouter.route("/:studentId") //localhost:8000/students/any
.patch(async(req,res)=>{
    //console.log(req.params)
    let studentId=req.params.studentId
    //console.log(studentId)
    let data=req.body
    //console.log(data)

    try {
        let result= await Student.findByIdAndUpdate(studentId,data);
        res.json({
            success:true,
            message:"Student updated successfully",
            result:result

        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

})
.delete(async(req,res)=>{
    let studentId=req.params.studentId
    try {
        let result= await Student.findByIdAndDelete(studentId)
        res.json({
            success:true,
            message:"Student Deleted successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

})

//Read Specific
.get(async(req,res)=>{
    let studentId=req.params.studentId
    try {
        let result=await Student.findById(studentId)
        res.json({
        success:true,
        message:"Student Read Successfully",
        result:result,
    })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message

        })     
    } 
})

export default studentRouter;