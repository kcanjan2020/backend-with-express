import e from "express";
import { Webuser } from "../model/model.js";

export let createWebuser = async (req, res) => {
  let data = req.body;
  //console.log(data);
  try {
    let result = await Webuser.create(data);
    res.json({
      success: true,
      message: "Webuser Created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readWebuser = async (req, res) => {
  let query = req.query;
  //   console.log(query);
  //   console.log(query.name)
  try {
    //let result = await Webuser.find({}); // Read all data from model Webuser

    // let result = await Webuser.find({ name: query.name });
    let result = await Webuser.find(query);
    res.json({
      success: true,
      message: "Webuser read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateWebuser = async (req, res) => {
  let webuserId = req.params.webuserId;
  //console.log(webuserId);
  let data = req.body;
  //console.log(data);
  try {
    let result = await Webuser.findByIdAndUpdate(webuserId, data);
    res.json({
      success: true,
      message: "Webuser updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readWebuserDetails = async (req, res) => {
  let webuserId = req.params.webuserId;
  try {
    let result = await Webuser.findById(webuserId);
    res.json({
      success: true,
      message: "Webuser read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteWebuserDetails = async (req, res) => {
  let webuserId = req.params.webuserId;
  try {
    let result = await Webuser.findByIdAndDelete(webuserId);
    res.json({
      success: true,
      message: "Webuser deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
