import express, { json } from "express";
import connectToMongoDb from "./src/databaseConnection/mongoDbConnection.js";
import studentRouter from "./src/router/studentRouter.js";
import teacherRouter from "./src/router/teacherRouter.js";
import { bookRouter } from "./src/router/bookRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import productRouter from "./src/router/productRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import cors from "cors";
import webUserRouter from "./src/router/webUserRouter.js";
import classRoomRouter from "./src/router/classRoomRouter.js";
let expressApp = express();
expressApp.use(json());
expressApp.use(cors()); //Normally we can not hit api from frontend =>it is used hit api from frontend
//Cors==> cross origin resources sharing

let port = 8000;
expressApp.listen(port, () => {
  console.log(`Express app is listening at port ${port}`);
});

connectToMongoDb();

expressApp.use("/students", studentRouter);
expressApp.use("/teachers", teacherRouter);
expressApp.use("/books", bookRouter);
expressApp.use("/trainees", traineeRouter);
expressApp.use("/webUsers", webUserRouter);
expressApp.use("/products", productRouter);
expressApp.use("/reviews", reviewRouter);
expressApp.use("/classrooms", classRoomRouter);
