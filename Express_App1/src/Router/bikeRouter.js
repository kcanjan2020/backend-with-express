import { Router } from "express";

let bikeRouter=Router();
bikeRouter.route("/")
.post(()=>{
    console.log("Bike post");
})
.get(()=>{
    console.log("Bike Get");
})
.patch(()=>{
    console.log("Bike Patch");
})
.delete(()=>{
    console.log("Bike Delete");
})

export default bikeRouter;