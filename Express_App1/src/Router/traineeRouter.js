import { Router } from "express";

let traineeRouter=Router();
traineeRouter.route("/")
.post(()=>{
    console.log({Success:true, Message:"School Created successfully"})
})
.get(()=>{
    console.log({success:true,message:"School Read successfully"})
})
.patch(()=>{
    console.log({success:true,message:"School Updated successfully"})
})
.delete(()=>{
    console.log({success:true,message:"School Deleted Successfully"})
})

export default traineeRouter;