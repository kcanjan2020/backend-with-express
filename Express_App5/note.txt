==>Database 
    -> mongoDb store  data in array of objects

=>CRUD Operation
 1.Create -> create(data)
 2.Read all -> find({})
 3.Read Specific -> findById(id)
 4.Update Specific -> findByIdAndUpdate(id,data)
 5.Delete Specific -> findByIdAndDelete(id)

 ==>.env 
->make .env at root directory   
->it is used to define variable
->how to define variable in .env
-> use uppercase convention
->while giving value do not use double quotes
-> every thing is string at .env
-> we need to install package dotenv=> npm i dotenv
-> used config() every time while used .env
->config()-> used to get variable from .env

==>.gitignore
->make .gitignore at root directory


==> Send mail : we need to install package: npm i nodemailer
Step:
login=> email and password=app password
to 
subject
body



//upload file
->every file must be place at static folder so that we could get image through link
-> to static folder
    ->expressApp.use(express.static("./public"));