import fs from "fs";
import pdf from "html-pdf";
import { Router } from "express";
import upload from "../middleware/uploadFile.js";
import { sendMail } from "../../utils/sendmail.js";
let htmlToPdfRouter = Router();

let uploadMultipleFile = async (req, res, next) => {
  try {
    let output = Date.now() + "output.pdf";

    console.log(req.files);
    let links = req.files.map((value, i) => {
      return `http://localhost:8000/${value.filename}`;
    });
    let filepath = req.files.map((value, i) => {
      return value.path;
    });
    console.log(...filepath);

    let html = fs.readFileSync(...filepath, "utf8");
    let options = { format: "Letter" };

    pdf.create(html, options).toFile(`./public/${output}`, function (err, res) {
      if (err) return console.log(err);
      console.log(res);
    });
    await sendMail({
      from: '"Anjan KC" <kcanjan2020@gmail.com>',
      to: ["kcanjan2020@gmail.com"],
      cc: ["kcanjan2020@gmail.com"],
      bcc: ["kcanjan2020@gmail.com"],
      //bcc is blind carbon copy
      attachments: [
        {
          //filename: "Anjan.jpg", // Replace with your desired filename
          path: `./public/${output}`, // Replace with the actual file path on your server
        },
      ],

      subject: "Document Conversion Confirmation",
      html: `<h1>Your document has been converted successfully.<h1>`,
    });
    res.json({
      success: true,
      message:
        " Your Html file is successfully converted to pdf Please check Your Email for  Confirmation",
    });
  } catch (error) {
    res.json({
      success: false,
      message: "Html to pdf conversion failed",
    });
  }
};

htmlToPdfRouter
  .route("/")
  .post(upload.array("document", 5), uploadMultipleFile);

export default htmlToPdfRouter;
