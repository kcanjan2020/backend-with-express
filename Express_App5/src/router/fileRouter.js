import fs from "fs";
import pdf from "html-pdf";
import { Router } from "express";
import upload from "../middleware/uploadFile.js";
let fileRouter = Router();

let uploadMultipleFile = (req, res, next) => {
  console.log(req.files);
  let links = req.files.map((value, i) => {
    return `http://localhost:8000/${value.filename}`;
  });
  res.json({
    success: true,
    message: "File Upload successfully",
    result: links,
  });
};
fileRouter.route("/").post(upload.array("document", 5), uploadMultipleFile);

export default fileRouter;
