import { sendMail } from "../../utils/sendmail.js";
import { Webuser } from "../model/model.js";
import bcrypt from "bcrypt";
export let createWebuser = async (req, res) => {
  let data = req.body;
  //console.log(data); // {ame: 'Anjan',age: 24 email: 'kcanjan2020@gmail.com',password: 'Password@123',phoneNumber: 9866904450, } => object
  try {
    let password = data.password;
    //console.log(password);
    let hasPassword = await bcrypt.hash(password, 10); // 10 means string hashing  2^10 times
    //console.log(hasPassword);
    data = {
      ...data,
      password: hasPassword,
    };
    //console.log(data);
    let result = await Webuser.create(data);
    //Send mail from google smtp
    await sendMail({
      from: '"Hello Display" <kcanjan2020@gmail.com>', //it is used to display text before email address
      to: [req.body.email], //=>send to specific mail
      subject: "My first system email",
      html: `<h1>Hello world:</h1>`,
    });

    /* 
    await sendMail({
      from: '"Fred Foo" <nitanthapa425@gmail.com>',
      to: ["kcanjan2020@gmail.com"],
      cc: ["kcanjan2020@gmail.com"],
      bcc: ["kcanjan2020@gmail.com"],
      //bcc is blind carbon copy
      attachments: [
        {
          filename: "Anjan.jpg", // Replace with your desired filename
          path: "C:/Users/ACER/OneDrive/Desktop/Backend-With-Express/Express_App5/src/controller/Anjan.jpg", // Replace with the actual file path on your server
        },
      ],

      subject: "this is subject",
      html: `<h1>Hello World<h1>`,
    });
 */
    res.json({
      success: true,
      message: "Webuser Created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readWebuser = async (req, res) => {
  let query = req.query;
  //   console.log(query);
  //   console.log(query.name)
  try {
    //let result = await Webuser.find({}); // Read all data from model Webuser

    // let result = await Webuser.find({ name: query.name });
    let result = await Webuser.find(query);
    res.json({
      success: true,
      message: "Webuser read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateWebuser = async (req, res) => {
  let webuserId = req.params.webuserId;
  //console.log(webuserId);
  let data = req.body;
  //console.log(data);
  try {
    let result = await Webuser.findByIdAndUpdate(webuserId, data);
    res.json({
      success: true,
      message: "Webuser updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readWebuserDetails = async (req, res) => {
  let webuserId = req.params.webuserId;
  try {
    let result = await Webuser.findById(webuserId);
    res.json({
      success: true,
      message: "Webuser read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteWebuserDetails = async (req, res) => {
  let webuserId = req.params.webuserId;
  try {
    let result = await Webuser.findByIdAndDelete(webuserId);
    res.json({
      success: true,
      message: "Webuser deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
