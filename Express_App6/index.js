import express, { json } from "express";
import connectToMongoDb from "./src/databaseConnection/mongoDbConnection.js";
import studentRouter from "./src/router/studentRouter.js";
import teacherRouter from "./src/router/teacherRouter.js";
import { bookRouter } from "./src/router/bookRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import productRouter from "./src/router/productRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import cors from "cors";
import webUserRouter from "./src/router/webUserRouter.js";
import classRoomRouter from "./src/router/classRoomRouter.js";
import { port } from "./src/config.js";
import bcrypt from "bcrypt";
import fileRouter from "./src/router/fileRouter.js";
import htmlToPdfRouter from "./src/router/htmltopdf.js";
let expressApp = express();
expressApp.use(json());
expressApp.use(cors()); //Normally we can not hit api from frontend =>it is used to hit api from frontend
//Cors==> cross origin resources sharing

expressApp.use(express.static("./public")); //every file must be place at static folder
expressApp.use(express.static("./src/router/static")); //every file must be place at static folder
expressApp.listen(port, () => {
  console.log(`Express app is listening at port ${port}`); // Here port is imported from config.js
});

connectToMongoDb();

expressApp.use("/students", studentRouter);
expressApp.use("/teachers", teacherRouter);
expressApp.use("/books", bookRouter);
expressApp.use("/trainees", traineeRouter);
expressApp.use("/webUsers", webUserRouter);
expressApp.use("/products", productRouter);
expressApp.use("/reviews", reviewRouter);
expressApp.use("/classrooms", classRoomRouter);
expressApp.use("/files", fileRouter);
expressApp.use("/htmltopdf", htmlToPdfRouter);

/* ==> Generate hasPassword
-->we need to download bcrypt package =>npm i bcrypt  

let password = "Password@123";
let hasPassword = await bcrypt.hash(password, 10); //10 means=> string is bcrypt 2^10 times
console.log(hasPassword);

==>login (matchPassword)
let isValidPassword = await bcrypt.compare(password, hasPassword);
console.log(isValidPassword);
*/


//File
let files = [
  {
    destination: "./public",
    filename: "abc.jpg",
  },
  {
    destination: "./public",
    filename: "anjan.jpg",
  },
  {
    destination: "./public",
    filename: "ram.jpg",
  },
];
let output = files.map((value, index) => {
  return `http://localhost:800/${value.filename}`;
});
console.log(output);
