import { Router } from "express";
import {
  createWebuser,
  deleteWebuserDetails,
  loginWebuser,
  myProfile,
  readWebuser,
  readWebuserDetails,
  updateWebuser,
} from "../controller/webUserController.js";

let webUserRouter = Router();
webUserRouter
  .route("/") //localhost:8000/webUsers
  .post(createWebuser)
  .get(readWebuser);

webUserRouter.route("/login").post(loginWebuser);
webUserRouter.route("/myprofile").get(myProfile);
//for update
webUserRouter
  .route("/:webuserId") //localhost:8000/webUsers/any
  .patch(updateWebuser)
  //Read Specific data
  .get(readWebuserDetails)
  //Delete specific data
  .delete(deleteWebuserDetails);

export default webUserRouter;
