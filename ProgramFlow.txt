==> mongoDb store  data in array of objects
   ->eg
    Student[
        {
            "_id": "6562b050e5abb022788d1610",
            "name": "Anjan",
            "age": 2567,
            "isMarried": false,
            "__v": 0
        },
        {
            "_id": "6562bb14c3b788c819cd8c1d",
            "name": "Sita",
            "age": 24,
            "isMarried": false,
            "__v": 0
        }
    ]


==> 1. Design Schema ()
     ->Defining object is call Schema
     -> let StudentSchema=Schema({object of objects})
==> 2. Design Model  (Model==>Table_Name like SQl Server)
      -> defining array is called Model
      -> model used modelName (ArrayName)  and Schema
      -> model name be singular and firstLetter is capital
      ->eg let Student=model("Student",Schema)
               (Student)     (Student) must be same(match)
==>3. Design Router 
     --> Router use model for (CRUD) operation